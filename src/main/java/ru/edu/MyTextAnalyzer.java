package ru.edu;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class MyTextAnalyzer implements TextAnalyzer {


    /**
     * Сбор статистики.
     */
    private TextStatistics textStatistics = new TextStatistics();

    /**
     * Анализ строки произведения.
     *
     * @param line - обрабатываемая строка
     */
    @Override
    public void analyze(final String line) {

        long charsCountWithoutSpace;
        long charsOnlyPunctuation;

        textStatistics.addCharsCount(line.length());

        charsCountWithoutSpace = charsCountWithoutSpaces(line);
        textStatistics.addCharsCountWithoutSpaces(charsCountWithoutSpace);

        textStatistics.addWordsCount(wordsCount(line));

        charsOnlyPunctuation = charsCountOnlyPunctuations(line);
        textStatistics.addCharsCountOnlyPunctuations(charsOnlyPunctuation);

    }

    /**
     * Получение сохраненной статистики.
     *
     * @return TextStatistic
     */
    @Override
    public TextStatistics getStatistic() {
        return textStatistics;
    }


    /**
     * Расчет числа символов без пробелов.
     * @param line - обрабатываемая строка
     * @return charsCountWithoutSpaces
     */
    private long charsCountWithoutSpaces(final String line) {

        long charsCountWithoutSpaces;
        long charsCountSpaces;

        if (line.length() == 0) {
            return 0;
        }

        String tempLine = line;
        tempLine = tempLine.replaceAll("\\s", "");

        charsCountSpaces = line.length() - tempLine.length();
        charsCountWithoutSpaces = line.length() - charsCountSpaces;
        return charsCountWithoutSpaces;
    }

    /**
     * Расчет количества слов.
     * @param line - обрабатываемая строка
     * @return count
     */
    private long wordsCount(final String line) {

        long count = 0;

        Pattern p = Pattern.compile("[a-zA-Zа-яА-Я]+");
        Matcher m = p.matcher(line);

        while (m.find()) {
            count++;
        }

        return count;
    }

    private long charsCountOnlyPunctuations(final String line) {

        long count = 0;
        String tempLine = line;

        Pattern p = Pattern.compile("[!,-./:;]+");
        Matcher m = p.matcher(tempLine);

        while (m.find()) {
            count++;
        }

        return count;

    }
}
