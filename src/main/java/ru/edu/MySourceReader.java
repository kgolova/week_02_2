package ru.edu;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class MySourceReader implements SourceReader {
    /**
     * переменная класса MySourceReader.
     */
    private File textFile;

    /**
     * Установка источника.
     * В реализации тут будет приходить путь до файла-источника
     *
     * @param source
     */
    @Override
    public void setup(final String source) {
        if (source == null) {
            throw new IllegalArgumentException("sourse is null");
        }

        textFile = new File(source);

        if (!textFile.exists()) {
            throw new IllegalArgumentException("sourse file is missing");
        }
    }

    /**
     * Метод для анализа источника.
     *
     * @param analyzer - логика подсчета статистики
     * @return - рассчитанная статистика
     */
    @Override
    public TextStatistics readSource(final TextAnalyzer analyzer) {

        if (analyzer == null) {
            throw new IllegalArgumentException("analyzer is null");
        }

        try (FileReader fr = new FileReader(textFile);
                BufferedReader br = new BufferedReader(fr)) {
            processSingleLine(analyzer, br);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

        return analyzer.getStatistic();
    }

    /**
     * Обработка строки.
     *
     * @param analyzer
     * @param br
     */
    private void processSingleLine(final TextAnalyzer analyzer,
                                   final BufferedReader br) throws IOException {
        String line;

        boolean isFirstLine = true;

        while ((line = br.readLine()) != null) {

            analyzer.analyze(isFirstLine ? line : "\n" + line);

            isFirstLine = false;
        }
        br.readLine();
    }
}
