package ru.edu;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.File;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.nio.charset.StandardCharsets;

public class MyStatisticReporter implements StatisticReporter {

    /**
     * переменная класса MyStatisticReporter.
     */
    private File file;

    /**
     * Установка файла приемника результатов.
     *
     *@param filePath
     */
    public MyStatisticReporter(final String filePath) {
        file = new File(filePath);
    }

    /**
     * Формирование отчета.
     *
     * @param statistics - данные статистики
     */
    @Override
    public void report(final TextStatistics statistics) {

        try (FileOutputStream os = new FileOutputStream(file);
             OutputStreamWriter sw = new OutputStreamWriter(os,
                     StandardCharsets.UTF_8);
             PrintWriter writer = new PrintWriter(sw)) {

            writer.println("Cлов: " + statistics.getWordsCount());
            writer.println("Символов: " + statistics.getCharsCount());
            writer.println("Символов без пробелов: "
                    + statistics.getCharsCountWithoutSpaces());
            writer.println("Знаков пунктуации: "
                    + statistics.getCharsCountOnlyPunctuations());

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
