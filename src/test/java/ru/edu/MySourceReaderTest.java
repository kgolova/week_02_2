package ru.edu;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


public class MySourceReaderTest {

    public static final String FILE = "./src/test/resources/input_text.txt";
    public static final String NOT_EXIST_FILE = "./src/test/resources/input_text1.txt";
    public static final String TEST_FILE = "./src/test/resources/test.txt";

    private final SourceReader reader = new MySourceReader();

    @Test
    public void setupNotEmptyLine() throws IllegalAccessException {

        reader.setup(FILE);

    }

    @Test(expected = IllegalArgumentException.class)
    public void setupNull() throws IllegalAccessException {

        reader.setup(null);

    }

    @Test(expected = IllegalArgumentException.class)
    public void setupNotExistFile() throws IllegalAccessException {

        reader.setup(NOT_EXIST_FILE);

    }

    @Test(expected = IllegalArgumentException.class)
    public void readSourceNull() throws IllegalAccessException {

        reader.readSource(null);

    }

    @Test
    public  void readSource() throws IllegalAccessException {

        TextAnalyzer analyzer = new TestAnalyzerText();

        reader.setup(TEST_FILE);

        TextStatistics statistics = reader.readSource(analyzer);

        assertNotNull(statistics);

        assertEquals(2, statistics.getWordsCount());

        // не разобралась почему IDEA предлагает выполнить следующим образом
        // а не так как рассматривали на вебинаре
        // analyzer.lines.get(0)
        assertEquals("QWERTY 123458 PTRTI", ((TestAnalyzerText) analyzer).lines.get(0));
        assertEquals("\nASDDFHHJ 78876545 kfkljbklc", ((TestAnalyzerText) analyzer).lines.get(1));

    }

    public static class TestAnalyzerText implements TextAnalyzer {

        public  List<String> lines = new ArrayList<>();

        /**
         * Анализ строки произведения.
         *
         * @param line
         */
        @Override
        public void analyze(String line) {
            lines.add(line);
        }

        /**
         * Получение сохраненной статистики.
         *
         * @return TextStatistic
         */
        @Override
        public TextStatistics getStatistic() {

            TextStatistics statistics = new TextStatistics();

            statistics.addWordsCount(lines.size());

            return statistics;

        }
    }
}