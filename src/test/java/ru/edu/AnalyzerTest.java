package ru.edu;

import org.junit.Test;
//import ru.edu.empty.EmptySourceReader;
//import ru.edu.empty.EmptyStatisticReporter;
//import ru.edu.empty.EmptyTextAnalyzer;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class AnalyzerTest {

    /**
     * По умолчанию задачи запускаются с рабочей директорией в корне проекта
     */
    public static final String FILE_PATH = "./src/test/resources/input_text.txt";
    public static final String FILE_PATH_RESALT = "./src/test/resources/result.txt";
    public static final int EXPECTED_WORDS = 41;
    public static final int EXPECTED_CHARS = 268;
    public static final int EXPECTED_CHARS_WO_SPACES = 218;
    public static final int EXPECTED_CHARS_ONLY_PUNCT = 7;

    /**
     * Нужно переключиться на вашу реализацию
     */
    private final TextAnalyzer analyzer = new MyTextAnalyzer();

    /**
     * Нужно переключиться на вашу реализацию
     */
    private final SourceReader reader = new MySourceReader();

    private final StatisticReporter reporter = new MyStatisticReporter(FILE_PATH_RESALT);


    @Test
    public void validation() throws IllegalAccessException {

        reader.setup(FILE_PATH);

        TextStatistics statistics = reader.readSource(analyzer);

        assertNotNull(statistics);

        assertEquals(EXPECTED_WORDS, statistics.getWordsCount());
        assertEquals(EXPECTED_CHARS, statistics.getCharsCount());
        assertEquals(EXPECTED_CHARS_WO_SPACES, statistics.getCharsCountWithoutSpaces());
        assertEquals(EXPECTED_CHARS_ONLY_PUNCT, statistics.getCharsCountOnlyPunctuations());

        List<String> topWords = statistics.getTopWords();

        reporter.report(statistics);
        System.out.println(statistics);

        assertEquals(0, topWords.size());

    }

}