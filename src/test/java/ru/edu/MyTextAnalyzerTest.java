package ru.edu;

import junit.framework.TestCase;
import org.junit.Test;

public class MyTextAnalyzerTest extends TestCase {

    @Test
    public void testAnalyze() {

        TextAnalyzer analyzer = new MyTextAnalyzer();

        analyzer.analyze("Шла Саша по шоссе");

        TextStatistics statistics = analyzer.getStatistic();

        assertEquals(4, statistics.getWordsCount());
        assertEquals(17, statistics.getCharsCount());
        assertEquals(0, statistics.getCharsCountOnlyPunctuations());
        assertEquals(14, statistics.getCharsCountWithoutSpaces());

    }

    @Test
    public void testAnalyzeMultyline() {

        TextAnalyzer analyzer = new MyTextAnalyzer();

        analyzer.analyze("Шла Саша по шоссе");
        analyzer.analyze("и не дошла:)");

        TextStatistics statistics = analyzer.getStatistic();

        assertEquals(7, statistics.getWordsCount());
        assertEquals(29, statistics.getCharsCount());
        assertEquals(1, statistics.getCharsCountOnlyPunctuations());
        assertEquals(24, statistics.getCharsCountWithoutSpaces());

    }

    @Test
    public void testLineEmpty(){

        TextAnalyzer analyzer = new MyTextAnalyzer();

        analyzer.analyze("");

        TextStatistics statistics = analyzer.getStatistic();

        assertEquals(0, statistics.getCharsCountWithoutSpaces());

    }

}